TEXFILE = main
texfiles := $(wildcard *.tex)
bibfiles := $(wildcard *.bib)

$(TEXFILE).pdf: $(texfiles) $(bibfiles) 
	@echo 'The IFI-front page needs the local ./ifikompendium'
	@echo 'do an export TEXINPUTS=./ifikompendium:$$TEXINPUTS'
	pdflatex $(TEXFILE) 
	bibtex   $(TEXFILE) 
	pdflatex $(TEXFILE) 
	pdflatex $(TEXFILE) 

clean:
	rm -f $(TEXFILE).pdf 
	rm -r *.log *.blg *.aux *.bbl *.lof *.lot *.toc *~



