\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Background and motivation}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Problem Definition}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Limitations}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Research Method}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Main Contributions}{3}{section.1.5}
\contentsline {section}{\numberline {1.6}Outline}{4}{section.1.6}
\contentsline {chapter}{\numberline {2}Background and Related work}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Virtualization}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Virtualization components}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Virtualization techniques}{6}{subsection.2.1.2}
\contentsline {subsubsection}{Full virtualization}{6}{section*.6}
\contentsline {subsubsection}{Hardware-assisted virtualization}{8}{section*.7}
\contentsline {subsubsection}{Paravirtualization}{9}{section*.8}
\contentsline {subsection}{\numberline {2.1.3}I/O virtualization}{9}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Current virtualization solutions}{10}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Xen}{11}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}KVM and Qemu}{12}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Linux}{13}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Memory management}{14}{subsection.2.3.1}
\contentsline {subsubsection}{Paging}{14}{section*.9}
\contentsline {subsubsection}{Page Tables and the MMU}{15}{section*.10}
\contentsline {subsection}{\numberline {2.3.2}Device drivers in Linux}{16}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Communication with I/O devices}{17}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Caching}{19}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}PCI Express}{20}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}PCI configuration space}{20}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Non-transparent bridge}{21}{subsection.2.4.2}
\contentsline {section}{\numberline {2.5}Software Infrastructure for Shared-Memory Cluster Interconnect (SISCI)}{22}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}API functionality}{22}{subsection.2.5.1}
\contentsline {subsubsection}{Memory segments}{23}{section*.11}
\contentsline {subsubsection}{Interrupts}{24}{section*.12}
\contentsline {subsubsection}{Error checking}{25}{section*.13}
\contentsline {subsection}{\numberline {2.5.2}Hardware}{25}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}Inter-VM communication}{26}{section.2.6}
\contentsline {section}{\numberline {2.7}Summary}{28}{section.2.7}
\contentsline {chapter}{\numberline {3}Design}{29}{chapter.3}
\contentsline {section}{\numberline {3.1}Design goals}{29}{section.3.1}
\contentsline {section}{\numberline {3.2}Choice of hypervisor}{30}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Performance comparison}{30}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Hypervisor environment}{31}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Features comparison}{31}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Exploring different designs}{32}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Passthrough}{32}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Virtio}{32}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Custom virtual device}{33}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Architecture}{35}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Qemu virtual device}{36}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Communication channel}{37}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Guest driver}{37}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Modified SISIC API}{38}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Discussion}{38}{subsection.3.4.5}
\contentsline {section}{\numberline {3.5}Summary}{39}{section.3.5}
\contentsline {chapter}{\numberline {4}Implementation}{41}{chapter.4}
\contentsline {section}{\numberline {4.1}Qemu virtual device}{41}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Mapping SISCI memory into device BARs}{43}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}BAR structure}{44}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Unmapping SISCI memory from device BARs}{45}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Interrupts}{47}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Managing SISCI descriptors and resources in the host}{49}{subsection.4.1.5}
\contentsline {section}{\numberline {4.2}Guest to host communication channel}{50}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Host side of communication}{51}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Guest side of communication}{54}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Protocol}{54}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Guest driver}{55}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Device initialization}{56}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Handling SISCI API requests}{56}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Mapping device memory into a SISCI application}{57}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Interrupt handling in the guest}{60}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Managing SISCI descriptors and resources in the guest}{61}{subsection.4.3.5}
\contentsline {section}{\numberline {4.4}Modified SISCI API}{61}{section.4.4}
\contentsline {section}{\numberline {4.5}Summary}{64}{section.4.5}
\contentsline {chapter}{\numberline {5}Evaluation and Discussion}{65}{chapter.5}
\contentsline {section}{\numberline {5.1}Evaluation environment}{65}{section.5.1}
\contentsline {section}{\numberline {5.2}Correctness}{66}{section.5.2}
\contentsline {section}{\numberline {5.3}Performance}{66}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Memory bandwidth}{67}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Memory latency}{73}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Interrupt latency}{74}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}Security}{76}{section.5.4}
\contentsline {section}{\numberline {5.5}Discussion}{77}{section.5.5}
\contentsline {chapter}{\numberline {6}Conclusion}{79}{chapter.6}
\contentsline {section}{\numberline {6.1}Summary}{79}{section.6.1}
\contentsline {section}{\numberline {6.2}Main Contributions}{80}{section.6.2}
\contentsline {section}{\numberline {6.3}Future work}{80}{section.6.3}
\contentsline {chapter}{\numberline {A}Source code}{83}{appendix.A}
