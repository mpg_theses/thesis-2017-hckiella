\contentsline {lstlisting}{\numberline {2.1}A cut down version of the file operations data structure}{17}{lstlisting.2.1}
\contentsline {lstlisting}{\numberline {4.1}The TypeInfo data structure}{42}{lstlisting.4.1}
\contentsline {lstlisting}{\numberline {4.2}Overriding the default Qemu PCI class functions}{42}{lstlisting.4.2}
\contentsline {lstlisting}{\numberline {4.3}Setting device properties when starting Qemu}{43}{lstlisting.4.3}
\contentsline {lstlisting}{\numberline {4.4}Initializing and registering the container for BAR2}{45}{lstlisting.4.4}
\contentsline {lstlisting}{\numberline {4.5}Adding a subregion to the BAR2 container}{45}{lstlisting.4.5}
\contentsline {lstlisting}{\numberline {4.6}Creating a SISCI interrupt with a callback}{48}{lstlisting.4.6}
\contentsline {lstlisting}{\numberline {4.7}The host function invoked when a SISCI interrupt is triggered}{49}{lstlisting.4.7}
\contentsline {lstlisting}{\numberline {4.8}Host structure for handling descriptors and associated resources}{50}{lstlisting.4.8}
\contentsline {lstlisting}{\numberline {4.9}Handling a request from the guest}{53}{lstlisting.4.9}
\contentsline {lstlisting}{\numberline {4.10}Handling an ioctl from the API requesting creation of a new segment.}{58}{lstlisting.4.10}
\contentsline {lstlisting}{\numberline {4.11}Guest driver structure for handling minor devices and associated resources}{62}{lstlisting.4.11}
\contentsline {lstlisting}{\numberline {4.12}Illustration of how the SISCI API has been modified in the guest}{64}{lstlisting.4.12}
